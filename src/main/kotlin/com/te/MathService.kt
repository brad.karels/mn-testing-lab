package com.te

interface MathService {
    fun compute(num: Int): Int
}